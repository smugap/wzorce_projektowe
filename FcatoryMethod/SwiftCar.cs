﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    public abstract class SwiftCar
    {
        public CarColor Color{get; private set;}
        protected SwiftCar(CarColor color)
        {
            this.Color = color;
        }
        public abstract double CalculatePrice();
    }
}

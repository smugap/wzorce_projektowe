﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            SwiftCar BlueBasicSwiftCar = SwiftCarFactory.CreateSwiftCar(SwiftCarType.BASIC, CarColor.BLUE);
            double bbsPrice = BlueBasicSwiftCar.CalculatePrice();
            Console.WriteLine("Price of " + nameof(BlueBasicSwiftCar) + ": " + bbsPrice);

            SwiftCar RedFeaturedSwiftCar = SwiftCarFactory.CreateSwiftCar(SwiftCarType.FEATURED, CarColor.RED);
            double rfscPrice = RedFeaturedSwiftCar.CalculatePrice();
            Console.WriteLine("Price of " + nameof(RedFeaturedSwiftCar) + ": " + rfscPrice);

            Console.ReadKey();
        }
    }
    public enum SwiftCarType
    {
        BASIC,
        FEATURED
    }
    public enum CarColor
    {
        BLACK,
        RED,
        BLUE
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    class Phone
    {
        public string Model { get; set; }
        public string Marka { get; set; }
        public string PrzekatnaEkranu { get; set; }
        public List<string> Accessories { get; set; }
        public Phone()
        {
            Accessories = new List<string>();
        }
        public void ShowInfo()
        {
            Console.WriteLine($"Marka: {Marka}");
            Console.WriteLine($"Model: {Model}");
            Console.WriteLine($"Przekątna Ekranu: {PrzekatnaEkranu}");
            Console.WriteLine($"Accessories:");
            foreach (var accesory in Accessories)
            {
                Console.WriteLine($"\t{accesory}");
            }
        }


    }
}

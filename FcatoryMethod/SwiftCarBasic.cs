﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    public class SwiftCarBasic : SwiftCar
    {
        public SwiftCarBasic(CarColor color) : base(color)
        {
        }

        public override double CalculatePrice()
        {
            double BasicPrice = 400000;
            return BasicPrice;
        }
    }
}

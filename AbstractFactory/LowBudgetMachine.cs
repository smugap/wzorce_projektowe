﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class LowBudgetMachine : IMachineFactory
    {
        public virtual IHardDisk GetHardDisk()
        {
            return new CheapDisk();
        }

        public virtual IProcesor GetProcessor()
        {
            return new CheapProcessor();
        }
    }
}

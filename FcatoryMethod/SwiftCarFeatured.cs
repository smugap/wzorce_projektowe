﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    public class SwiftCarFeatured : SwiftCar
    {
        public SwiftCarFeatured(CarColor color) : base(color)
        {
        }

        public override double CalculatePrice()
        {
            double basicPrice = 400000;
            double otherEquipmentCosts = 200000;
            return basicPrice + otherEquipmentCosts;
        }
    }
}

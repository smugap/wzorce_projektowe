﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
    [Serializable]
    class Color :ColorPrototype
    {
        private int _red;
        private int _green;
        private int _blue;

        public Color(int red, int green, int blue)
        {
            _red = red;
            _green = green;
            _blue = blue;
        }

        //płytka kopia
        public override ColorPrototype Clone()
        {
            Console.WriteLine($"Cloning color RGB: {_red,3} {_blue,3} {_green,3}");

            return this.MemberwiseClone() as ColorPrototype;
        }
    }
   
}

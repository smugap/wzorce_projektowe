﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    //zamiast interfejsu można klase abstrakcyjną jeśli chcemy miec stattus i nie musimy miec wszystkich tych metod
    interface IVehicleBuilder
    {
        void SetModel();
        void SetEngine();
        void SetAccessories();

        Vehicle GetVehicle();
    }
}

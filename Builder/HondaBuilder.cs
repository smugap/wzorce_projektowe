﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    class HondaBuilder : IVehicleBuilder
    {
        Vehicle objVehicle = new Vehicle();
        public Vehicle GetVehicle()
        {
            return objVehicle;
        }

        public void SetAccessories()
        {
            objVehicle.Accessories.Add("Seat Cover");
            objVehicle.Accessories.Add("Helmet");
            objVehicle.Accessories.Add("Rear Mirror");
        }

        public void SetEngine()
        {
            objVehicle.Engine = "2.0";
        }

        public void SetModel()
        {
            objVehicle.Model = "Honda";
        }
    }
}

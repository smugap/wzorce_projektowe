﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    class Vehicle
    {
        public string Model { get; set; }
        public string Engine { get; set; }
        public List<string> Accessories { get; set; }
        public Vehicle()
        {
            Accessories = new List<string>();
        }
        public void ShowInfo()
        {
            Console.WriteLine($"Model: {Model}");
            Console.WriteLine($"Engine: {Engine}");
            Console.WriteLine($"Accessories:");
            foreach (var accesory in Accessories)
            {
                Console.WriteLine($"\t{accesory}");
            }
        }
    }
}

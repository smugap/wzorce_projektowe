﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    class Program
    {
        //jak chce nowa marke to zrobic np FerrariBuilder;
        static void Main(string[] args)
        {
            var vehicleCreator = new VehicleCreator(new HondaBuilder());
            vehicleCreator.CreateVehicle();
            var vehicle = vehicleCreator.GetVehicle();
            vehicle.ShowInfo();
            Console.WriteLine("-------------------------------------------------");

            


            var phoneCreator = new PhoneCreator(new SamsungPhoneBuilder());
            phoneCreator.Construct();
            var phone = phoneCreator.getPhone();
            phone.ShowInfo();





            Console.ReadKey();
        }
    }
}

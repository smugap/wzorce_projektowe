﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    class SamsungPhoneBuilder : IPhoneBuilder
    {
        Phone obj = new Phone();
        public Phone getPhone()
        {
            return obj;
        }

        public void SetAccessories()
        {
            obj.Accessories.Add("Etui");
            obj.Accessories.Add("Ładowarka");
        }

        public void SetModel()
        {
            obj.Model = "Galaxy S8";
        }
        public void  SetMarka()
        {
            obj.Marka = "Samsung";
        }
        public void SetPrzekatna()
        {
            obj.PrzekatnaEkranu = "6.0\"";
        }
    }
}

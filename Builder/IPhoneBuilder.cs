﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    interface IPhoneBuilder
    {
        void SetModel();
        void SetPrzekatna();
        void SetAccessories();
        void SetMarka();
        Phone getPhone();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    //np gdy modyfikujemy sprzet
    class AverageBudgetMachine :LowBudgetMachine
    {
        public override IProcesor GetProcessor()
        {
            return new ExpensiveProcessor();
        }
    }
}

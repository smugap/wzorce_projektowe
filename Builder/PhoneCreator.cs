﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    class PhoneCreator
    {
        private readonly IPhoneBuilder obj;
        public PhoneCreator(IPhoneBuilder rhs)
        {
            obj = rhs;
        }
        public void Construct()
        {
            obj.SetMarka();
            obj.SetModel();
            obj.SetPrzekatna();
            obj.SetAccessories();
        }
        public Phone getPhone()
        {
            return obj.getPhone();
        }
    }
}

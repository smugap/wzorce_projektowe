﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
    class ColorManager
    {
        Dictionary<string,ColorPrototype> _colors = new Dictionary<string, ColorPrototype>();

        //indexer
        public ColorPrototype this[string key]
        {
            get { return _colors[key]; }
            set { _colors[key] = value; }
        }

    }
}

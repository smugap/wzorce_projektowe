﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class HighBudgetMachine : IMachineFactory
    {
        public IHardDisk GetHardDisk()
        {
            return new ExpensiveDisk();
        }

        public IProcesor GetProcessor()
        {
            return new ExpensiveProcessor();
        }
    }
}

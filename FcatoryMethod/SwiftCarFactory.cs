﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    public static class SwiftCarFactory
    {
        public static SwiftCar CreateSwiftCar(SwiftCarType carType, CarColor carColor)
        {
            SwiftCar car = null;
            switch (carType)
            {
                case SwiftCarType.BASIC:
                    car = new SwiftCarBasic(carColor);
                    break;
                case SwiftCarType.FEATURED:
                    car = new SwiftCarFeatured(carColor);
                    break;
            }
            return car;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    public class SingletonSimpleThreadSafe
    {
        private static readonly Lazy<SingletonSimpleThreadSafe> lazy = new Lazy<SingletonSimpleThreadSafe>(() => new SingletonSimpleThreadSafe());
        private  string _variable { get; set; }
        public static SingletonSimpleThreadSafe Instance => lazy.Value;

        public override string ToString()
        {
            return _variable;
        }

        public void ChangeValue(string rhs)
        {
            _variable = rhs;
        }
        private SingletonSimpleThreadSafe()
        {
            _variable = "Witaj świecie! ;)";
        }
    }
}

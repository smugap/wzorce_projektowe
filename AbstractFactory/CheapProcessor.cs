﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class CheapProcessor : IProcesor
    {
        public void PerformOperation()
        {
            Console.WriteLine("Operation will perform slowly");
        }
    }
}

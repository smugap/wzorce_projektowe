﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            var p = new Program();

            //Look: http://csharpindepth.com/Articles/General/Singleton.aspx

            SingletonSimpleThreadSafe s1 = SingletonSimpleThreadSafe.Instance;
            SingletonSimpleThreadSafe s2 = SingletonSimpleThreadSafe.Instance;
            s2.ChangeValue("Hello World !!!");
            Console.WriteLine(s1);
            Console.WriteLine(s2);








            Console.ReadKey();
        }
    }
}

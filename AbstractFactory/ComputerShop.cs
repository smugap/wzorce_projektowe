﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class ComputerShop
    {
        IMachineFactory _category;
        public ComputerShop(IMachineFactory category)
        {
            _category = category;
        }
        public void AssembleMachine()
        {
            IProcesor processor = _category.GetProcessor();
            IHardDisk hdd = _category.GetHardDisk();

            processor.PerformOperation();
            hdd.StoreData();
        }
    }
}
